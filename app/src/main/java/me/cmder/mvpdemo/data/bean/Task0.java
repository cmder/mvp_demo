package me.cmder.mvpdemo.data.bean;

/**
 * Created by Yao Ximing on 2018/1/9.
 */

public class Task0 {
    private String data;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
