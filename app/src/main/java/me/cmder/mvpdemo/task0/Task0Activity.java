package me.cmder.mvpdemo.task0;

import android.app.Activity;
import android.os.Bundle;

import me.cmder.mvpdemo.R;
import me.cmder.mvpdemo.util.ActivityUtils;

public class Task0Activity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Task0Fragment task0Fragment = Task0Fragment.newInstance();
        ActivityUtils.addFragmentToActivity(getFragmentManager(), task0Fragment, R.id.contentFrame);

        new Task0Presenter(task0Fragment);
    }
}
