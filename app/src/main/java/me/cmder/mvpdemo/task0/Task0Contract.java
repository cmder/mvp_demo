package me.cmder.mvpdemo.task0;

import android.app.Activity;
import android.content.Context;

import me.cmder.mvpdemo.base.BasePresenter;
import me.cmder.mvpdemo.base.BaseView;
import me.cmder.mvpdemo.data.bean.Task0;

/**
 * Created by Yao Ximing on 2018/1/9.
 */

public interface Task0Contract {

    interface View extends BaseView<Presenter> {

        void showData(Task0 task0);

        Context obtainContext();

    }

    interface Presenter extends BasePresenter {

        void fetchData();

    }
}
