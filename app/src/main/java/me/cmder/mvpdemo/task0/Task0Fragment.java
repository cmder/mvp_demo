package me.cmder.mvpdemo.task0;


import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import me.cmder.mvpdemo.R;
import me.cmder.mvpdemo.data.bean.Task0;

/**
 * A simple {@link Fragment} subclass.
 */
public class Task0Fragment extends Fragment implements Task0Contract.View{

    private Task0Contract.Presenter mPresenter;

    private TextView txt;
    private Button btn;
    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }

    public Task0Fragment() {
        // Required empty public constructor
    }

    public static Task0Fragment newInstance() {
        return new Task0Fragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_task0, container, false);
        txt = view.findViewById(R.id.txt);
        btn = view.findViewById(R.id.btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.fetchData();
            }
        });
        return view;
    }

    @Override
    public void setPresenter(Task0Contract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.start();
    }

    @Override
    public void showData(Task0 task0) {
        txt.setText(task0.getData());
    }

    @Override
    public Context obtainContext() {
        return this.getActivity();
    }
}
