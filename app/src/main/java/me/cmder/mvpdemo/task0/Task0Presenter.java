package me.cmder.mvpdemo.task0;

import me.cmder.mvpdemo.data.bean.Task0;

/**
 * Created by Yao Ximing on 2018/1/9.
 */

public class Task0Presenter implements Task0Contract.Presenter {

    private Task0Contract.View mTask0View;

    public Task0Presenter(Task0Contract.View mTask0View) {
        this.mTask0View = mTask0View;
        mTask0View.setPresenter(this);
    }

    @Override
    public void start() {
        Task0 task0 = new Task0();
        task0.setData("initial state");
        mTask0View.showData(task0);
    }

    @Override
    public void fetchData() {
        // TODO: 2018/1/9 get data from model layer
        Task0 task0 = new Task0();
        task0.setData("hello world");
        mTask0View.showData(task0);
    }
}
